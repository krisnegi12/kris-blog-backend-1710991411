package com.caseStudy.eCart.Controller;

import com.caseStudy.eCart.Repository.BlogRepository;
import com.caseStudy.eCart.Repository.UserRepository;
import com.caseStudy.eCart.Service.CurrentUserService;
import com.caseStudy.eCart.model.Blog;
import com.caseStudy.eCart.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class BlogController {
    @Autowired
    BlogRepository blogRepository;
    @Autowired
    private CurrentUserService currentUserService;
    @Autowired
    private UserRepository userRepository;
    @PostMapping("/addblog")
    public Blog addblog(Principal principal,@Valid @RequestBody Blog blog)
    {
        int userid=currentUserService.getuserid(principal);
        Users user=userRepository.findById(userid).get();
        blog.setImg("./assets/newprod.jpg");
        blog.setUser(user);
        blog.setDate(new Date());
        return blogRepository.save(blog);
    }
    @GetMapping("/getAllBlogs")
    public List<Blog> getAllBlogs()
    {
        return blogRepository.findAllByOrderByDateDesc();
    }
    @GetMapping("/blog/{id}")
    public Optional<Blog> getBlogById(@PathVariable(value="id") int blogId)
    {
        return blogRepository.findById(blogId);
    }
    @GetMapping("/blog/search/{search}")
    public List<Blog> getBlogBySearch(@PathVariable(value="search") String blogSearch)
    {
        Optional<Users> user=userRepository.findByEmail(blogSearch);
        return blogRepository.findByNameOrDescriptionOrContentOrUserOrderByDateDesc(blogSearch,blogSearch,blogSearch,user);
    }
    @GetMapping("/blog/searchbydate/lasthour")
    public List<Blog> getBlogByLastHour()
    {
        Date d=new Date();
        Date d1=new Date(d.getTime()-3600000);
        return blogRepository.findByDateBetweenOrderByDateDesc(d1,d);
    }
    @GetMapping("/blog/searchbydate/today")
    public List<Blog> getBlogByToday()
    {
        Date d=new Date();
        Date d1=new Date(d.getTime()-86400000);
        return blogRepository.findByDateBetweenOrderByDateDesc(d1,d);
    }
    @GetMapping("/blog/searchbydate/thisweek")
    public List<Blog> getBlogByThisWeek()
    {
        Date d=new Date();
        Date d1=new Date(d.getTime()-604800000);
        return blogRepository.findByDateBetweenOrderByDateDesc(d1,d);
    }
    @GetMapping("/blog/searchbydate/thismonth")
    public List<Blog> getBlogByThisMonth()
    {
        Date d=new Date();
        Date d1=new Date(d.getTime()-(long)(2.629746*Math.pow(10,9)));
        return blogRepository.findByDateBetweenOrderByDateDesc(d1,d);
    }
    @GetMapping("/blog/searchbydate/thisyear")
    public List<Blog> getBlogByThisYear()
    {
        Date d=new Date();
        Date d1=new Date(d.getTime()-(long)(3.1556952*Math.pow(10,10)));
        return blogRepository.findByDateBetweenOrderByDateDesc(d1,d);
    }
    @PutMapping("/editblog/{id}")
    public Blog editBlog(@PathVariable(value="id") int blogId,@Valid @RequestBody Blog newblog)
    {
        Blog b=blogRepository.findById(blogId).get();
        b.setName(newblog.getName());
        b.setDescription(newblog.getDescription());
        b.setContent(newblog.getContent());
        b.setAccess(newblog.isAccess());
        return blogRepository.save(b);
    }
    @GetMapping("/deleteblog/{id}")
    public Blog deleteBlog(@PathVariable(value="id") int blogId)
    {
        Blog b=blogRepository.findById(blogId).get();
        blogRepository.deleteById(blogId);
        return b;
    }
}
