package com.caseStudy.eCart.Controller;

import com.caseStudy.eCart.Repository.BlogRepository;
import com.caseStudy.eCart.Repository.CommentsRepository;
import com.caseStudy.eCart.Repository.UserRepository;
import com.caseStudy.eCart.Service.CurrentUserService;
import com.caseStudy.eCart.model.Blog;
import com.caseStudy.eCart.model.Comments;
import com.caseStudy.eCart.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class CommentsController {
    @Autowired
    BlogRepository blogRepository;
    @Autowired
    private CurrentUserService currentUserService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CommentsRepository commentsRepository;
    @GetMapping("/addcomment/{blogid}/{content}")
    public Comments addcomment(@PathVariable(value="blogid") int blogid, @PathVariable(value="content") String content, Principal principal)
    {
        int userid=currentUserService.getuserid(principal);
        Users user=userRepository.findById(userid).get();
        Blog blog=blogRepository.findById(blogid).get();
        Comments com=new Comments();
        com.setBlog(blog);
        com.setUser(user);
        com.setDate(new Date());
        com.setContent(content);
        return commentsRepository.save(com);
    }
    @GetMapping("/deletecomment/{id}")
    public Comments deletecomment(@PathVariable(value="id") int commentId)
    {
        Comments c=commentsRepository.findById(commentId).get();
        commentsRepository.deleteById(commentId);
        return c;
    }
    @GetMapping("/getBlogComments/{id}")
    public List<Comments> getBlogComments(@PathVariable(value="id") int blogId)
    {
        Blog blog=blogRepository.findById(blogId).get();
        return commentsRepository.findByBlogOrderByDateDesc(blog);
    }
}
