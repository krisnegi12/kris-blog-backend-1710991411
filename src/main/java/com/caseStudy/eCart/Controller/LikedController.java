package com.caseStudy.eCart.Controller;

import com.caseStudy.eCart.Repository.CommentsRepository;
import com.caseStudy.eCart.Repository.LikedRepository;
import com.caseStudy.eCart.Repository.UserRepository;
import com.caseStudy.eCart.Service.CurrentUserService;
import com.caseStudy.eCart.model.Comments;
import com.caseStudy.eCart.model.Liked;
import com.caseStudy.eCart.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class LikedController {
    @Autowired
    private CurrentUserService currentUserService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CommentsRepository commentsRepository;
    @Autowired
    private LikedRepository likedRepository;
    @GetMapping("/clicklike/{id}")
    public Liked clicklike(@PathVariable(value="id") int commentId, Principal principal)
    {
        int userid=currentUserService.getuserid(principal);
        Users user=userRepository.findById(userid).get();
        Comments c=commentsRepository.findById(commentId).get();
        if(likedRepository.findByCommentsAndUser(c,user).isPresent())
        {
            Liked l=likedRepository.findByCommentsAndUser(c,user).get();
            l.setLikestate(true);
            return likedRepository.save(l);
        }
        else
        {
            Liked l=new Liked();
            l.setUser(user);
            l.setComments(c);
            l.setLikestate(true);
            return likedRepository.save(l);
        }
    }
    @GetMapping("/clickdislike/{id}")
    public Liked clickdislike(@PathVariable(value="id") int commentId, Principal principal)
    {
        int userid=currentUserService.getuserid(principal);
        Users user=userRepository.findById(userid).get();
        Comments c=commentsRepository.findById(commentId).get();
        if(likedRepository.findByCommentsAndUser(c,user).isPresent())
        {
            Liked l=likedRepository.findByCommentsAndUser(c,user).get();
            l.setLikestate(false);
            return likedRepository.save(l);
        }
        else
        {
            Liked l=new Liked();
            l.setUser(user);
            l.setComments(c);
            l.setLikestate(false);
            return likedRepository.save(l);
        }
    }
    @GetMapping("/getLikedByUser")
    public List<Liked> getLikedByUser(Principal principal)
    {
        int userid=currentUserService.getuserid(principal);
        Users user=userRepository.findById(userid).get();
        return likedRepository.findByUser(user);
    }
}
