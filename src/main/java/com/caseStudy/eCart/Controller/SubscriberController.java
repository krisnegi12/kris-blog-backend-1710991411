package com.caseStudy.eCart.Controller;

import com.caseStudy.eCart.Repository.BlogRepository;
import com.caseStudy.eCart.Repository.SubscriberRepository;
import com.caseStudy.eCart.Repository.UserRepository;
import com.caseStudy.eCart.Service.CurrentUserService;
import com.caseStudy.eCart.model.Blog;
import com.caseStudy.eCart.model.Subscriber;
import com.caseStudy.eCart.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class SubscriberController {
    @Autowired
    private SubscriberRepository subscriberRepository;
    @Autowired
    BlogRepository blogRepository;
    @Autowired
    private CurrentUserService currentUserService;
    @Autowired
    private UserRepository userRepository;
    @GetMapping("/subscribe/{id}")
    public Subscriber subscribe(@PathVariable(value="id") int subscribedId, Principal principal)
    {
        int userid=currentUserService.getuserid(principal);
        Users user=userRepository.findById(userid).get();
        Users subscribed=userRepository.findById(subscribedId).get();
        Subscriber sub=new Subscriber();
        sub.setUser(user);
        sub.setSubscribed(subscribed);
        return subscriberRepository.save(sub);
    }
    @GetMapping("/unsubscribe/{id}")
    public Subscriber unsubscribe(@PathVariable(value="id") int subscribedId, Principal principal)
    {
        int userid=currentUserService.getuserid(principal);
        Users user=userRepository.findById(userid).get();
        Users subscribed=userRepository.findById(subscribedId).get();
        Subscriber sub=subscriberRepository.findByUserAndSubscribed(user,subscribed).get();
        subscriberRepository.delete(sub);
        return sub;
    }
    @GetMapping("/checksubscribe/{id}")
    public boolean checksubscribe(@PathVariable(value="id") int subscribedId, Principal principal)
    {
        int userid=currentUserService.getuserid(principal);
        Users user=userRepository.findById(userid).get();
        Users subscribed=userRepository.findById(subscribedId).get();
        if(subscriberRepository.findByUserAndSubscribed(user,subscribed).isPresent())
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    @GetMapping("/usersfollowing/{id}")
    public List<Subscriber> getUsersFollowing(@PathVariable(value="id") int userId)
    {
        Users user=userRepository.findById(userId).get();
        return subscriberRepository.findByUser(user);
    }
    @GetMapping("/userfollower/{id}")
    public List<Subscriber> getUserFollower(@PathVariable(value="id") int userId)
    {
        Users user=userRepository.findById(userId).get();
        return subscriberRepository.findBySubscribed(user);
    }
    @GetMapping("/userblogs/{id}")
    public List<Blog> getUserBlogs(@PathVariable(value="id") int userId)
    {
        Users user=userRepository.findById(userId).get();
        return blogRepository.findByUserOrderByDateDesc(user);
    }
    @GetMapping("/newsfeed")
    public List<Blog> newsfeed(Principal principal)
    {
        int userid=currentUserService.getuserid(principal);
        Users user=userRepository.findById(userid).get();
        List<Blog> feed=new ArrayList<>();
        List<Subscriber> subitems=subscriberRepository.findByUser(user);
        for(Subscriber sub:subitems)
        {
            feed.add(blogRepository.findFirstByUserOrderByDateDesc(sub.getSubscribed()));
        }
        return feed;
    }
}
