package com.caseStudy.eCart.Controller;

import com.caseStudy.eCart.Repository.UserRepository;
import com.caseStudy.eCart.Service.CurrentUserService;
import com.caseStudy.eCart.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200",allowedHeaders = "*")
public class UserController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    private CurrentUserService currentUserService;

    @PostMapping("/createUser")
    public Users createUser(@Valid @RequestBody Users user) {
        user.setActive("true");
        user.setRole("writer");
        return userRepository.save(user);
    }
    @GetMapping("/validateUser")
    public String validateUser()
    {
        return "\"user successfully authenticated\"";
    }
    @GetMapping("/getuser")
    public Users getUser(Principal principal)
    {
        int userid=currentUserService.getuserid(principal);
        Users user=userRepository.findById(userid).get();
        return user;
    }
    @PutMapping("/edituser")
    public Users editUser(Principal principal,@Valid @RequestBody Users newuser)
    {
        int userid=currentUserService.getuserid(principal);
        Users user=userRepository.findById(userid).get();
        user.setFullName(newuser.getFullName());
        user.setUserName(newuser.getUserName());
        user.setRole(newuser.getRole());
        user.setEmail(newuser.getEmail());
        user.setPassword(newuser.getPassword());
        return userRepository.save(user);
    }
    @GetMapping("/user/{id}")
    public Users getUserById(@PathVariable(value="id") int userId)
    {
        Users user=userRepository.findById(userId).get();
        return user;
    }
}
