package com.caseStudy.eCart.Repository;

import com.caseStudy.eCart.model.Blog;
import com.caseStudy.eCart.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface BlogRepository extends JpaRepository<Blog,Integer> {
    List<Blog> findAllByOrderByDateDesc();
    /*List<Blog> findByName(String name);
    List<Blog> findByDescription(String description);
    List<Blog> findByContent(String content);*/
    List<Blog> findByUserOrderByDateDesc(Users user);
    List<Blog> findByNameOrDescriptionOrContentOrUserOrderByDateDesc(String name, String description, String content, Optional<Users> user);
    Blog findFirstByUserOrderByDateDesc(Users user);
    List<Blog> findByDateBetweenOrderByDateDesc(Date d1,Date d);
}
