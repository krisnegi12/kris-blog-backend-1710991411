package com.caseStudy.eCart.Repository;

import com.caseStudy.eCart.model.Comments;
import com.caseStudy.eCart.model.Liked;
import com.caseStudy.eCart.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LikedRepository extends JpaRepository<Liked,Integer> {
    Optional<Liked> findByCommentsAndUser(Comments c, Users user);
    List<Liked> findByUser(Users user);
}
