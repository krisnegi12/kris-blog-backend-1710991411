package com.caseStudy.eCart.Repository;

import com.caseStudy.eCart.model.Subscriber;
import com.caseStudy.eCart.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubscriberRepository extends JpaRepository<Subscriber,Integer> {
    List<Subscriber> findByUser(Users user);
    Optional<Subscriber> findByUserAndSubscribed(Users user, Users subscribed);
    List<Subscriber> findBySubscribed(Users user);
}
