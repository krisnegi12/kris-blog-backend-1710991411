package com.caseStudy.eCart.Service;

import com.caseStudy.eCart.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public class CurrentUserService {
    @Autowired
    private UserRepository userRepository;


    public int getuserid(Principal principal)
    {
        String email=principal.getName();
        int id=userRepository.findByEmail(email).get().getId();
        return id;
    }

}
