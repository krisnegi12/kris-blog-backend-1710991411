package com.caseStudy.eCart.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name="liked")
public class Liked implements Serializable {
    @Column(name="id")
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    private Comments comments;
    @ManyToOne
    private Users user;
    @Column(name="likestate")
    @NotNull
    private boolean likestate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Comments getComments() {
        return comments;
    }

    public void setComments(Comments comments) {
        this.comments = comments;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public boolean isLikestate() {
        return likestate;
    }

    public void setLikestate(boolean likestate) {
        this.likestate = likestate;
    }
}
