package com.caseStudy.eCart.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="subscriber")
public class Subscriber implements Serializable {
    @Column(name="id")
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    private Users user;
    @ManyToOne
    private Users subscribed;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Users getSubscribed() {
        return subscribed;
    }

    public void setSubscribed(Users subscribed) {
        this.subscribed = subscribed;
    }
}
